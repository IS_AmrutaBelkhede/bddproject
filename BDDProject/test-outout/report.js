$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/Users/ashis/Downloads/CucumberSeleniumFramework-master/CucumberSeleniumFramework-master/src/main/java/Features/loginWithHooks.feature");
formatter.feature({
  "line": 1,
  "name": "My Store Login with Hooks Feature",
  "description": "",
  "id": "my-store-login-with-hooks-feature",
  "keyword": "Feature"
});
formatter.before({
  "duration": 5795808700,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "My Store Login Login with Hooks Test Scenario",
  "description": "",
  "id": "my-store-login-with-hooks-feature;my-store-login-login-with-hooks-test-scenario",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "user is already on Login Page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "title of login page is My Store",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "click on sign in link",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user enter username and password",
  "rows": [
    {
      "cells": [
        "username",
        "password"
      ],
      "line": 9
    },
    {
      "cells": [
        "amrutab@info.com",
        "amrutab"
      ],
      "line": 10
    }
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "user clicks on Sign in button",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "user is on home page",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginWithHooksStepDefinition.user_already_on_login_page()"
});
formatter.result({
  "duration": 14476923100,
  "status": "passed"
});
formatter.match({
  "location": "LoginWithHooksStepDefinition.titleOfLoginPage()"
});
formatter.result({
  "duration": 34730000,
  "status": "passed"
});
formatter.match({
  "location": "LoginWithHooksStepDefinition.clickOnSignInLink()"
});
formatter.result({
  "duration": 10206167600,
  "status": "passed"
});
formatter.match({
  "location": "LoginWithHooksStepDefinition.userEntersUsernameAndPassword(DataTable)"
});
formatter.result({
  "duration": 434379400,
  "status": "passed"
});
formatter.match({
  "location": "LoginWithHooksStepDefinition.userClicksOnSignInButton()"
});
formatter.result({
  "duration": 11296441900,
  "status": "passed"
});
formatter.match({
  "location": "LoginWithHooksStepDefinition.user_is_on_hopme_page()"
});
formatter.result({
  "duration": 9125900,
  "status": "passed"
});
formatter.after({
  "duration": 741767000,
  "status": "passed"
});
});