package MyRunner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


	@RunWith(Cucumber.class)
	@CucumberOptions(
			features = "C:\\Users\\ashis\\Downloads\\CucumberSeleniumFramework-master\\CucumberSeleniumFramework-master\\src\\main\\java\\Features\\loginWithHooks.feature",
			glue={"stepDefinitions"},
			format= {"pretty","html:test-outout", "json:json_output/cucumber.json", "junit:junit_xml/cucumber.xml"}, 
			monochrome = true, 
			dryRun = false 
			)
	 
	public class TestRunner {
	 
	}
	
	
	

