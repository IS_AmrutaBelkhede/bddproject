/*package stepDefinitions;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginDataTableStepDefinition{

	WebDriver driver;

	
	 @Given("^user is already on Login Page$")
	 public void user_already_on_login_page(){
	 System.setProperty("webdriver.chrome.driver","C:\\Users\\ashis\\Downloads\\CucumberSeleniumFramework-master\\CucumberSeleniumFramework-master\\Server\\chromedriver.exe");
	 driver = new ChromeDriver();
	 driver.get("http://automationpractice.com/index.php");
	 driver.manage().window().maximize();
	 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	 }
	
	
	 @When("^title of login page is My Store$")
	 public void titleOfLoginPage(){
	 String title = driver.getTitle();
	 Assert.assertEquals("My Store", title);
	 }
	
	
	 @Then("^click on sign in link$")
	 public void clickOnSignInLink(){
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 driver.findElement(By.linkText("Sign in")).click();
	}
		
	 @Then("^user enter username and password$")
	 public void userEntersUsernameAndPassword(DataTable credentials){
		 List<List<String>> data = credentials.raw();
	 	 driver.findElement(By.name("email")).sendKeys(data.get(0).get(0));
	 	 driver.findElement(By.name("passwd")).sendKeys(data.get(0).get(1));
	 }
	 
	 @Then("^user clicks on Sign in button$")
	 public void userClicksOnSignInButton() {
	 WebElement loginBtn =
	 driver.findElement(By.xpath("//button[@id='SubmitLogin']"));
	 JavascriptExecutor js = (JavascriptExecutor)driver;
	 js.executeScript("arguments[0].click();", loginBtn);
	 }
		
	 @Then("^user is on home page$")
	 public void user_is_on_hopme_page(){
	 String title = driver.getTitle();
	 Assert.assertEquals("My account - My Store", title);
	 }

	 @Then("^Close the browser$")
	 public void close_the_browser(){
		 driver.quit();
	 }

}
*/