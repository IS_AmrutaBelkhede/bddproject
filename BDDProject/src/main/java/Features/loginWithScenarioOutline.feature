Feature: My Store Login Feature
#Scenario outline with Examples Keyword
Scenario Outline: My Store Login Login Test Scenario

Given user is already on Login Page
When title of login page is My Store
Then click on sign in link and user enters "<username>" and "<password>"
Then user clicks on Sign in button
Then user is on home page
Then Close the browser

Examples:
	| username 					| password |
	| amrutab@info.com  | amrutab  |
	