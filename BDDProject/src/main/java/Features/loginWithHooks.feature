Feature: My Store Login with Hooks Feature

Scenario: My Store Login Login with Hooks Test Scenario

Given user is already on Login Page
When title of login page is My Store
Then click on sign in link
Then user enter username and password
| username | password |
| amrutab@info.com | amrutab |

Then user clicks on Sign in button
Then user is on home page
